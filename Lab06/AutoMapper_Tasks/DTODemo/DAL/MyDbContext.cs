﻿using System.Data.Entity;

namespace DAL
{
    public class MyDbContext : DbContext
    {
        public MyDbContext() : base("DTODemo")
        {
            Database.SetInitializer(new MyDbInitializer());
        }
        public DbSet<User> Users { get; set; }
    }
}
