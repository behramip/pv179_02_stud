﻿namespace DAL
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public bool IsSingle { get; set; }
        public string Mail { get; set; }
    }
}
