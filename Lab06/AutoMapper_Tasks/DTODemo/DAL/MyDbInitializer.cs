﻿using System.Data.Entity;

namespace DAL
{
    public class MyDbInitializer : DropCreateDatabaseAlways<MyDbContext>
    {
        protected override void Seed(MyDbContext context)
        {
            context.Users.Add(new User
            {
                Name = "Peter",
                Surname = "Hrivnak",
                Age = 42,
                IsSingle = false,
                Mail = "peto@gmail.com"
            });
            base.Seed(context);
        }
    }
}
