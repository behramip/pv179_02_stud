﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DemoEshop.BusinessLayer.DataTransferObjects;
using DemoEshop.BusinessLayer.DataTransferObjects.Filters;
using DemoEshop.BusinessLayer.QueryObjects.Common;
using DemoEshop.DataAccessLayer.EntityFramework.Entities;
using DemoEshop.Infrastructure.Query;
using DemoEshop.Infrastructure.Query.Predicates;
using DemoEshop.Infrastructure.Query.Predicates.Operators;

namespace DemoEshop.BusinessLayer.QueryObjects
{
    public class ProductQueryObject : QueryObjectBase<ProductDto, Product, ProductFilterDto, IQuery<Product>>
    {
        public ProductQueryObject(IMapper mapper, IQuery<Product> query) : base(mapper, query) { }

        protected override IQuery<Product> ApplyWhereClause(IQuery<Product> query, ProductFilterDto filter)
        {
            // TODO...
           
            throw new NotImplementedException();
        }
    }
}
