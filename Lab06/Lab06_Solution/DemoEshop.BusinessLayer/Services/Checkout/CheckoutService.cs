﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DemoEshop.BusinessLayer.DataTransferObjects;
using DemoEshop.BusinessLayer.DataTransferObjects.Enums;
using DemoEshop.BusinessLayer.Services.Checkout.PriceCalculators;
using DemoEshop.BusinessLayer.Services.Common;
using DemoEshop.DataAccessLayer.EntityFramework.Entities;
using DemoEshop.Infrastructure;

namespace DemoEshop.BusinessLayer.Services.Checkout
{
    public class CheckoutService : ServiceBase, ICheckoutService
    {
        private readonly IRepository<Order> orderRepository;
        private readonly IRepository<OrderItem> orderItemRepository;
        private readonly IRepository<Customer> customerRepository;
        private readonly IRepository<Product> productRepository;

        /// <summary>
        /// Registered price calculators
        /// </summary>
        private readonly IEnumerable<IPriceCalculator> priceCalculators;

        public CheckoutService(IMapper mapper, IEnumerable<IPriceCalculator> priceCalculators, IRepository<Order> orderRepository,
            IRepository<OrderItem> orderItemRepository, IRepository<Customer> customerRepository, IRepository<Product> productRepository) 
            : base(mapper)
        {
            this.orderRepository = orderRepository;
            this.orderItemRepository = orderItemRepository;
            this.customerRepository = customerRepository;
            this.productRepository = productRepository;
            this.priceCalculators = priceCalculators;
        }

        /// <summary>
        /// Persists order together with all related data
        /// </summary>
        /// <param name="createOrderDto">wrapper for order, orderItems, customer and coupon</param>
        public async Task<Guid> ConfirmOrderAsync(OrderCreateDto createOrderDto)
        {
            // TODO...

            throw new NotImplementedException();
        }

        /// <summary>
        /// Calculates total price for all order items
        /// </summary>
        /// <param name="orderItems">all order items</param>
        /// <returns>Total price for given items</returns>
        public decimal CalculateTotalPrice(ICollection<OrderItemDto> orderItems)
        {
            // TODO...

            throw new NotImplementedException();
        }
    }
}
