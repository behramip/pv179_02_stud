﻿using System.Collections.Generic;

namespace DemoEshop.BusinessLayer.DataTransferObjects
{
    public class OrderCreateDto
    {
        public OrderDto OrderDto { get; set; }

        public IEnumerable<OrderItemDto> OrderItems { get; set; } = new List<OrderItemDto>();
    }
}
