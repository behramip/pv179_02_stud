﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Sending.Attachments;
using Sending.Senders;

namespace Sending
{
    public class Installer : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<Facebook>()
                    .LifestyleTransient());

            container.Register(
                Component.For<ISender>()
                    .ImplementedBy<RealSender>()
                    .LifestyleTransient());

            container.Register(
                Component.For<IAttachment>()
                    .ImplementedBy<FileAttachment>()
                    .LifestyleTransient());

            /*
            container.Register(
                Component.For<ISender>()
                    .Instance(new RealSender(new FileAttachment())));*/
        }
    }
}
