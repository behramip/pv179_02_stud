﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sending.Attachments
{
    public interface IAttachment
    {
        string Show();
    }
}
