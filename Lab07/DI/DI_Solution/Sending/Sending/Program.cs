﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Windsor;
using Sending.Senders;

namespace Sending
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = new WindsorContainer();
            container.Install(new Installer());

            var app = container.Resolve<Facebook>();
            app.SendMessage();
        }
    }
}
