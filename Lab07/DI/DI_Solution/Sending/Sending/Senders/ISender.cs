﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sending.Senders
{
    public interface ISender
    {
        int SentMessages { get; }
        void Send();
    }
}
