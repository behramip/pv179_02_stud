﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sending.Senders;

namespace Sending
{
    class Facebook
    {
        private readonly ISender sender;

        public Facebook(ISender sender)
        {
            this.sender = sender;
        }

        public void SendMessage()
        {
            sender.Send();
        }
    }
}
