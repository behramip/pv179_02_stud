﻿using System;
using Sending.Attachments;

namespace Sending.Senders{
    class RealSender
    {
        public int SentMessages { get; private set; }

        private readonly IAttachment attachment;

        public RealSender(IAttachment attachment)
        {
            this.attachment = attachment;
        }
        public void Send()
        {
            SentMessages++;
            Console.WriteLine("To MUNI: Hello, " + attachment.Show());
        }
    }
}