﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sending.Senders
{
    class TestSender
    {
        public int SentMessages { get; private set; }
        public void Send()
        {
            SentMessages++;
            Console.WriteLine("To me: Hello");
        }
    }
}
