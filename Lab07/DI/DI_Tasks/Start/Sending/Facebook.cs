﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sending.Senders;

namespace Sending
{
    class Facebook
    {
        private readonly TestSender sender;

        public Facebook()
        {
            sender = new TestSender();
        }

        public void SendMessage()
        {
            sender.Send();
        }
    }
}
