﻿using System;
using DemoEshop.BusinessLayer.DataTransferObjects;
using DemoEshop.BusinessLayer.DataTransferObjects.Enums;

namespace DemoEshop.BusinessLayer.Services.Checkout.PriceCalculators
{
    public class PercentagePriceCalculator : IPriceCalculator
    {
        public DiscountType DiscountType { get; } = DiscountType.Percentage;

        public decimal CalculatePrice(OrderItemDto orderItem)
        {
            var pricePerSingleProduct = Math.Round(orderItem.Product.Price * (decimal)(1 - orderItem.Product.DiscountPercentage / 100.0));
            return pricePerSingleProduct * orderItem.Quantity;
        }
    }
}