using System;
using System.Threading.Tasks;
using DemoEshop.BusinessLayer.DataTransferObjects;

namespace DemoEshop.BusinessLayer.Services.Reservation
{
    public interface IReservationService
    {
        /// <summary>
        /// Performs product reservation in order to ensure
        /// simultaneous orders can be satisfied
        /// </summary>
        /// <param name="productReservation">Product reservation details containing 
        ///   customer reserving the product, amount, and expiration</param>
        /// <returns>true if reservation is successfull, otherwise false</returns>
        Task<bool> ReserveProduct(ProductReservationDto productReservation);

        /// <summary>
        /// Performs product release 
        /// (typically when user confirms or cancels the order)
        /// </summary>
        /// <param name="customerId">ID of customer who reserved the product</param>
        /// <param name="productIds">IDs of the product to release</param>
        void ReleaseProductReservations(Guid customerId, params Guid[] productIds);

        /// <summary>
        /// Performs product expedition by decreasing product 
        /// stored units and releasing corresponding reservations
        /// </summary>
        /// <param name="customerId">Id of customer whose reserved product should be dispatched</param>
        Task DispatchProduct(Guid customerId);

        /// <summary>
        /// Performs order of required product from distributor
        /// </summary>
        /// <param name="productId">id of product to order</param>
        /// <param name="quantity">Number of units to order</param>
        Task OrderProductFromDistributor(Guid productId, int quantity);

        /// <summary>
        /// Gets number of currently available (free)
        /// units for product
        /// </summary>
        /// <param name="productId">ID of the product for which product availability should be checked</param>
        /// <returns>Number of free units for this product</returns>
        Task<int> GetCurrentlyAvailableUnits(Guid productId);
    }
}