﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DemoEshop.BusinessLayer.DataTransferObjects;
using DemoEshop.BusinessLayer.DataTransferObjects.Filters;
using DemoEshop.BusinessLayer.Facades.Common;
using DemoEshop.BusinessLayer.Services.Checkout;
using DemoEshop.BusinessLayer.Services.Reservation;
using DemoEshop.BusinessLayer.Services.Sales;
using DemoEshop.Infrastructure.UnitOfWork;

namespace DemoEshop.BusinessLayer.Facades
{
    public class OrderFacade : FacadeBase
    {
        private readonly ISalesService salesService;
        private readonly ICheckoutService checkoutService;
        private readonly IReservationService productReservationService;

        public OrderFacade(IUnitOfWorkProvider unitOfWorkProvider, ICheckoutService checkoutService, ISalesService salesService, 
            IReservationService productReservationService) : base(unitOfWorkProvider)
        {
            this.checkoutService = checkoutService;
            this.salesService = salesService;
            this.productReservationService = productReservationService;
        }

        #region OrderManagement

        /// <summary>
        /// Persists order together with all related data
        /// </summary>
        /// <param name="createOrderDto">wrapper for order, orderItems, customer and coupon</param>
        /// <param name="confirmationAction">Action to perform when uow successfully performs after commit action</param>
        public async Task<Guid> ConfirmOrderAsync(OrderCreateDto createOrderDto, Action confirmationAction)
        {
            // TODO...

            throw new NotImplementedException();
        } 
        
        /// <summary>
        /// Calculates total price for all order items (overall coupon discount is included)
        /// </summary>
        /// <param name="orderItems">all order items</param>
        /// <returns>Total price for given items</returns>
        public decimal CalculateTotalPrice(ICollection<OrderItemDto> orderItems)
        {
            // TODO...

            throw new NotImplementedException();
        } 
        
        /// <summary>
        /// Gets all orders according to filter and required page
        /// </summary>
        /// <param name="filter">order filter</param>
        /// <returns>All orders</returns>
        public async Task<IEnumerable<OrderDto>> GetOrdersAsync(OrderFilterDto filter)
        {
            // TODO...

            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets all OrderItems from specified order
        /// </summary>
        /// <param name="filter">order item filter</param>
        /// <returns>all order items from specified order</returns>
        public async Task<IEnumerable<OrderItemDto>> ListItemsFromOrderAsync(OrderItemFilterDto filter)
        {
            using (UnitOfWorkProvider.Create())
            {
                return await salesService.ListOrderItemsAsync(filter);
            }
        }
        
        #endregion

        #region ProductsReservationManagement

        /// <summary>
        /// Performs product reservation in order to ensure
        /// simultaneous orders can be satisfied
        /// </summary>
        /// <param name="productReservation">Product reservation details containing 
        /// customer reserving the product, amount, productID and expiration</param>
        /// <returns>true if reservation is successfull, otherwise false</returns>
        public async Task<bool> ReserveProductAsync(ProductReservationDto productReservation)
        {
            using (UnitOfWorkProvider.Create())
            {
                return await productReservationService.ReserveProduct(productReservation);
            }
        }

        /// <summary>
        /// Performs product release 
        /// (typically when user confirms or cancels the order)
        /// </summary>
        /// <param name="customerId">ID of customer who reserved the product</param>
        /// <param name="productIds">IDs of the product to release</param>
        public void ReleaseReservations(Guid customerId, params Guid[] productIds)
        {
            using (UnitOfWorkProvider.Create())
            {
                productReservationService.ReleaseProductReservations(customerId, productIds);
            }
        }
        
        /// <summary>
        /// Gets number of currently available (free)
        /// units for product belonging to product with specified ID
        /// </summary>
        /// <param name="productId">ID of the product for which product availability should be checked</param>
        /// <returns>Number of free units for this product</returns>
        public async Task<int> GetCurrentlyAvailableUnitsAsync(Guid productId)
        {
            return await productReservationService.GetCurrentlyAvailableUnits(productId);
        }

        /// <summary>
        /// Performs order of required product from distributor
        /// </summary>
        /// <param name="productId">id of product to order</param>
        /// <param name="quantity">Number of units to order</param>
        public async Task OrderProductFromDistributorAsync(Guid productId, int quantity)
        {
            await productReservationService.OrderProductFromDistributor(productId, quantity);
        }

        #endregion
    }
}
