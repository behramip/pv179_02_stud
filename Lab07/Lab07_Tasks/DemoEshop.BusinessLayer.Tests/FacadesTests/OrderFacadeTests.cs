﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using DemoEshop.BusinessLayer.DataTransferObjects;
using DemoEshop.BusinessLayer.DataTransferObjects.Filters;
using DemoEshop.BusinessLayer.Facades;
using DemoEshop.BusinessLayer.QueryObjects.Common;
using DemoEshop.BusinessLayer.Services.Checkout;
using DemoEshop.BusinessLayer.Services.Checkout.PriceCalculators;
using DemoEshop.BusinessLayer.Services.Reservation;
using DemoEshop.BusinessLayer.Services.Sales;
using DemoEshop.BusinessLayer.Tests.FacadesTests.Common;
using DemoEshop.DataAccessLayer.EntityFramework.Entities;
using DemoEshop.Infrastructure;
using DemoEshop.Infrastructure.Query;
using Moq;
using NUnit.Framework;

namespace DemoEshop.BusinessLayer.Tests.FacadesTests
{
    [TestFixture]
    public class OrderFacadeTests
    {
        [Test]
        public async Task ConfirmOrderAsync_SingleReservation_ExecutesCorrectly()
        {
            var productId = Guid.NewGuid();
            var customerId = Guid.NewGuid();
            const int storedAmount = 10;
            const int reservedAmount = 3;
            var returnedProduct = new Product{ Id = productId, StoredUnits = storedAmount };
            var returnedProductDto = new ProductDto { Id = productId, StoredUnits = storedAmount };
            var reservation = new ProductReservationDto
            {
                CustomerId = customerId,
                ProductId = productId,
                ReservedAmount = reservedAmount,
                Expiration = DateTime.Now.Add(new TimeSpan(1, 0, 0))
            };
            var orderCreateDto = new OrderCreateDto
            {
                OrderDto = new OrderDto { Id = Guid.NewGuid(), CustomerId = customerId, Issued = DateTime.Now, TotalPrice = 8000},
                OrderItems = new List<OrderItemDto>
                {
                    new OrderItemDto{Id = Guid.NewGuid(), Product = returnedProductDto, Quantity = reservedAmount}
                }
            };
            

            // TODO...
        }

        [Test]
        public async Task GetCurrentlyAvailableUnits_WithNoReservations_ReturnsCorrectResult()
        {
            const int storedUnits = 3;
            var productId = Guid.NewGuid();
            
            // TODO...
        }

        [TestCase(1, 3, 1)]
        [TestCase(3, 3, 3)]
        [TestCase(3, 1, 0)]
        public async Task ReserveProduct_SingleReservation_ReturnsCorrectResult(int reservedAmount, int storedUnits, int expectedReservedAmount)
        {
            var productId = Guid.NewGuid();
            var customerId = Guid.NewGuid();
            var productReservation = new ProductReservationDto
            {
                CustomerId = customerId,
                ProductId = productId,
                Expiration = DateTime.Now.Add(new TimeSpan(1,0,0)),
                ReservedAmount = reservedAmount
            };
            
            // TODO...
        }

       
        /// <summary>
        /// Returns OrderFacade with just reservation service fully configured
        /// </summary>
        private static OrderFacade CreateOrderFacadeForReservationTesting(Mock<IRepository<Product>> productRepositoryMock = null)
        {
            var uowMock = FacadeMockManager.ConfigureUowMock();
            var mapper = FacadeMockManager.ConfigureRealMapper();
            var reservationService = new ReservationService(mapper, productRepositoryMock?.Object ?? new Mock<IRepository<Product>>().Object);
            var orderFacade = new OrderFacade(uowMock.Object, null, null, reservationService);
            return orderFacade;
        }

        /// <summary>
        /// Returns OrderFacade with all services fully configured
        /// </summary>
        private static OrderFacade CreateOrderFacade(Mock<IRepository<Product>> productRepositoryMock, Mock<QueryObjectBase<OrderItemDto, OrderItem, OrderItemFilterDto, IQuery<OrderItem>>> orderItemQueryMock, Mock<QueryObjectBase<OrderDto, Order, OrderFilterDto, IQuery<Order>>> orderQueryMock, Mock<IRepository<Order>> orderRepositoryMock, Mock<IRepository<OrderItem>> orderItemRepositoryMock, Mock<IRepository<Customer>> customerRepositoryMock, ReservationService productReservationService = null)
        {
            var uowMock = FacadeMockManager.ConfigureUowMock();
            var mapper = FacadeMockManager.ConfigureRealMapper();
            var salesService = new SalesService(mapper, orderItemQueryMock.Object, orderQueryMock.Object);
            var checkoutService = new CheckoutService(mapper,new List<IPriceCalculator>{new PercentagePriceCalculator(), new Quantity3Plus1PriceCalculator()}, orderRepositoryMock.Object, orderItemRepositoryMock.Object, customerRepositoryMock.Object, productRepositoryMock.Object);
            var reservationService = productReservationService ?? new ReservationService(mapper, productRepositoryMock.Object);
            var orderFacade = new OrderFacade(uowMock.Object, checkoutService, salesService, reservationService);
            return orderFacade;
        }
    }
}
