﻿using System.Threading.Tasks;
using DemoEshop.Infrastructure.UnitOfWork;

namespace DemoEshop.BusinessLayer.Tests.FacadesTests.Common
{
    /// <summary>
    /// Fake unit of work
    /// </summary>
    internal class StubUow : UnitOfWorkBase
    {
        protected override Task CommitCore()
        {
            return Task.Delay(15);
        }

        public override void Dispose()
        {
        }
    }
}
