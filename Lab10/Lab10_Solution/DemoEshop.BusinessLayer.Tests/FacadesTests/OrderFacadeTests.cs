﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using DemoEshop.BusinessLayer.DataTransferObjects;
using DemoEshop.BusinessLayer.DataTransferObjects.Filters;
using DemoEshop.BusinessLayer.Facades;
using DemoEshop.BusinessLayer.QueryObjects.Common;
using DemoEshop.BusinessLayer.Services.Checkout;
using DemoEshop.BusinessLayer.Services.Checkout.PriceCalculators;
using DemoEshop.BusinessLayer.Services.Reservation;
using DemoEshop.BusinessLayer.Services.Sales;
using DemoEshop.BusinessLayer.Tests.FacadesTests.Common;
using DemoEshop.DataAccessLayer.EntityFramework.Entities;
using DemoEshop.Infrastructure;
using DemoEshop.Infrastructure.Query;
using Moq;
using NUnit.Framework;

namespace DemoEshop.BusinessLayer.Tests.FacadesTests
{
    [TestFixture]
    public class OrderFacadeTests
    {
        [Test]
        public async Task ConfirmOrderAsync_SingleReservation_ExecutesCorrectly()
        {
            var productId = Guid.NewGuid();
            var customerId = Guid.NewGuid();
            const int storedAmount = 10;
            const int reservedAmount = 3;
            var returnedProduct = new Product{ Id = productId, StoredUnits = storedAmount };
            var returnedProductDto = new ProductDto { Id = productId, StoredUnits = storedAmount };
            var orderCreateDto = new OrderCreateDto
            {
                OrderDto = new OrderDto { Id = Guid.NewGuid(), CustomerId = customerId, Issued = DateTime.Now, TotalPrice = 8000},
                OrderItems = new List<OrderItemDto>
                {
                    new OrderItemDto{Id = Guid.NewGuid(), Product = returnedProductDto, Quantity = reservedAmount}
                }
            };
            var mockManager = new FacadeMockManager();
            var productRepositoryMock = mockManager.ConfigureGetAndUpdateRepositoryMock(returnedProduct, nameof(Product.StoredUnits));
            var customerRepositoryMock = mockManager.ConfigureGetRepositoryMock(new Customer{Id = customerId});
            var orderRepositoryMock = mockManager.ConfigureCreateRepositoryMock<Order>(nameof(Order.CustomerId));
            var orderItemRepositoryMock = mockManager.ConfigureRepositoryMock<OrderItem>();
            var orderQueryMock = mockManager.ConfigureQueryObjectMock<OrderDto, Order, OrderFilterDto>(null);
            var orderItemQueryMock = mockManager.ConfigureQueryObjectMock<OrderItemDto, OrderItem, OrderItemFilterDto>(null);
            var reservationService = new ReservationService(FacadeMockManager.ConfigureRealMapper(), productRepositoryMock.Object);
            await reservationService.ReserveProduct(new ProductReservationDto
            {
                CustomerId = customerId,
                ProductId = productId,
                ReservedAmount = reservedAmount,
                Expiration = DateTime.Now.Add(new TimeSpan(1, 0, 0))
            });

            var orderFacade = CreateOrderFacade(productRepositoryMock, orderItemQueryMock, orderQueryMock, orderRepositoryMock, orderItemRepositoryMock, customerRepositoryMock, reservationService);

            orderFacade.ConfirmOrderAsync(orderCreateDto, () => Debug.WriteLine("Order confirmed.")).Wait();

            Assert.AreEqual(storedAmount - reservedAmount, mockManager.CapturedUpdatedStoredUnits);
            Assert.AreEqual(customerId, mockManager.CapturedCreatedId);
        }

        [Test]
        public void ConfirmOrderAsync_MultipleConcurrentReservations_ExecutesCorrectly()
        {
            var productId = Guid.NewGuid();
            var customerId = Guid.NewGuid();
            const int storedAmount = 100;
            const int reservedAmount = 1;
            const int numberOfReservations = 10;
            var returnedProduct = new Product { Id = productId, StoredUnits = storedAmount };
            var returnedProductDto = new ProductDto { Id = productId, StoredUnits = storedAmount };
            var orderCreateDto = new OrderCreateDto
            {
                OrderDto = new OrderDto { Id = Guid.NewGuid(), CustomerId = customerId, Issued = DateTime.Now, TotalPrice = 8000 },
                OrderItems = new List<OrderItemDto>
                {
                    new OrderItemDto{Id = Guid.NewGuid(), Product = returnedProductDto, Quantity = reservedAmount}
                }
            };
            
            var mockManager = new FacadeMockManager();
            var productRepositoryMock = mockManager.ConfigureGetAndUpdateRepositoryMock(returnedProduct, nameof(Product.StoredUnits));
            var customerRepositoryMock = mockManager.ConfigureGetRepositoryMock(new Customer { Id = customerId });
            var orderRepositoryMock = mockManager.ConfigureCreateRepositoryMock<Order>(nameof(Order.CustomerId));
            var orderItemRepositoryMock = mockManager.ConfigureRepositoryMock<OrderItem>();
            var orderQueryMock = mockManager.ConfigureQueryObjectMock<OrderDto, Order, OrderFilterDto>(null);
            var orderItemQueryMock = mockManager.ConfigureQueryObjectMock<OrderItemDto, OrderItem, OrderItemFilterDto>(null);
            var reservationService = new ReservationService(FacadeMockManager.ConfigureRealMapper(), productRepositoryMock.Object);

            async Task ReservationAction(Guid cId, int reservedUnits) => await reservationService.ReserveProduct(new ProductReservationDto
            {
                CustomerId = cId,
                ProductId = productId,
                ReservedAmount = reservedUnits,
                Expiration = DateTime.Now.Add(new TimeSpan(1, 0, 0))
            });

            var tasks = new Task[10];
            for (var i = 0; i < numberOfReservations - 1; i++)
            {
                var amount = i;
                tasks[i] = Task.Run(() => ReservationAction(Guid.NewGuid(), amount));
            }
            tasks[numberOfReservations - 1] = Task.Run(() => ReservationAction(customerId, reservedAmount));
            Task.WaitAll(tasks);
            
            var orderFacade = CreateOrderFacade(productRepositoryMock, orderItemQueryMock, orderQueryMock, orderRepositoryMock, orderItemRepositoryMock, customerRepositoryMock, reservationService);

            orderFacade.ConfirmOrderAsync(orderCreateDto, () => Debug.WriteLine("Order confirmed.")).Wait();

            Assert.AreEqual(storedAmount - reservedAmount, mockManager.CapturedUpdatedStoredUnits);
            Assert.AreEqual(customerId, mockManager.CapturedCreatedId);
        }

        [TestCase(2000, 3000, 0, 0, 2, 3)]
        [TestCase(1000,1000,10,0,10,1)]
        public void CalculateTotalPrice_HavingMultipleOrderItems_ReturnsCorrectResult(int firstProductPrice, int secondProductPrice, int firstDiscountPercentage, int secondDiscountPercentage, int firstProductQuantity, int secondProductQuantity)
        {
            var orderItems = new List<OrderItemDto>
            {
                new OrderItemDto{Product = new ProductDto{DiscountPercentage = firstDiscountPercentage, Price = firstProductPrice}, Quantity = firstProductQuantity},
                new OrderItemDto{Product = new ProductDto{DiscountPercentage = secondDiscountPercentage, Price = secondProductPrice}, Quantity = secondProductQuantity}
            };

            var mockManager = new FacadeMockManager();
            var productRepositoryMock = mockManager.ConfigureRepositoryMock<Product>();
            var customerRepositoryMock = mockManager.ConfigureRepositoryMock<Customer>();
            var orderRepositoryMock = mockManager.ConfigureRepositoryMock<Order>();
            var orderItemRepositoryMock = mockManager.ConfigureRepositoryMock<OrderItem>();
            var orderQueryMock = mockManager.ConfigureQueryObjectMock<OrderDto, Order, OrderFilterDto>(null);
            var orderItemQueryMock = mockManager.ConfigureQueryObjectMock<OrderItemDto, OrderItem, OrderItemFilterDto>(null);

            var orderFacade = CreateOrderFacade(productRepositoryMock, orderItemQueryMock, orderQueryMock, orderRepositoryMock, orderItemRepositoryMock, customerRepositoryMock);

            var totalPrice = orderFacade.CalculateTotalPrice(orderItems);
            decimal CalculatePriceForOrderItem(int price, int discount, int quantity)
            {
                return (decimal) (price * (discount > 0 ? 1 - (discount / 100.0) : 1) * quantity);
            }

            var expectedPrice = CalculatePriceForOrderItem(firstProductPrice, firstDiscountPercentage, firstProductQuantity) +
                                CalculatePriceForOrderItem(secondProductPrice, secondDiscountPercentage, secondProductQuantity);
            Assert.AreEqual(expectedPrice, totalPrice);
        }


        [Test]
        public async Task GetCurrentlyAvailableUnits_WithNoReservations_ReturnsCorrectResult()
        {
            const int storedUnits = 3;
            var productId = Guid.NewGuid();
            var productRepositoryMock = new FacadeMockManager().ConfigureGetRepositoryMock(new Product { StoredUnits = storedUnits });
            var productFacade = CreateOrderFacadeForReservationTesting(productRepositoryMock);
            var actualAvaiableUnits = await productFacade.GetCurrentlyAvailableUnitsAsync(productId);
            Assert.AreEqual(storedUnits, actualAvaiableUnits);
        }

        [TestCase(1, 3, 1)]
        [TestCase(3, 3, 3)]
        [TestCase(3, 1, 0)]
        public async Task ReserveProduct_SingleReservation_ReturnsCorrectResult(int reservedAmount, int storedUnits, int expectedReservedAmount)
        {
            var productId = Guid.NewGuid();
            var customerId = Guid.NewGuid();
            var productReservation = new ProductReservationDto
            {
                CustomerId = customerId,
                ProductId = productId,
                Expiration = DateTime.Now.Add(new TimeSpan(1,0,0)),
                ReservedAmount = reservedAmount
            };
            var productRepositoryMock = new FacadeMockManager().ConfigureGetRepositoryMock(new Product{StoredUnits = storedUnits });
            var productFacade = CreateOrderFacadeForReservationTesting(productRepositoryMock);
            await productFacade.ReserveProductAsync(productReservation);
            var actualReservedUnits = storedUnits - await productFacade.GetCurrentlyAvailableUnitsAsync(productId);
            Assert.AreEqual(expectedReservedAmount, actualReservedUnits);
        }

        [Test]
        public async Task ReleaseReservations_SingleReservation_ReturnsCorrectResult()
        {
            const int reservedAmount = 1;
            const int storedUnits = 3;
            var productId = Guid.NewGuid();
            var customerId = Guid.NewGuid();
            var productReservation = new ProductReservationDto
            {
                CustomerId = customerId,
                ProductId = productId,
                Expiration = DateTime.Now.Add(new TimeSpan(1, 0, 0)),
                ReservedAmount = reservedAmount
            };
            var productRepositoryMock = new FacadeMockManager().ConfigureGetRepositoryMock(new Product { StoredUnits = storedUnits });
            var productFacade = CreateOrderFacadeForReservationTesting(productRepositoryMock);

            await productFacade.ReserveProductAsync(productReservation);
            productFacade.ReleaseReservations(customerId);

            var actualReservedUnits = storedUnits - await productFacade.GetCurrentlyAvailableUnitsAsync(productId);
            Assert.AreEqual(0, actualReservedUnits);
        }

        [Test]
        public async Task OrderProductFromDistributor_Condition_ReturnsCorrectResult()
        {
            const int orderedAmount = 3;
            const int storedUnits = 7;
            var productId = Guid.NewGuid();
            var productRepositoryMock = new FacadeMockManager().ConfigureGetRepositoryMock(new Product { StoredUnits = storedUnits });
            var productFacade = CreateOrderFacadeForReservationTesting(productRepositoryMock);

            await productFacade.OrderProductFromDistributorAsync(productId, orderedAmount);
            var actualStoredUnits = await productFacade.GetCurrentlyAvailableUnitsAsync(productId);

            Assert.AreEqual(storedUnits + orderedAmount, actualStoredUnits);
        }

        private static OrderFacade CreateOrderFacadeForReservationTesting(Mock<IRepository<Product>> productRepositoryMock = null)
        {
            var uowMock = FacadeMockManager.ConfigureUowMock();
            var mapper = FacadeMockManager.ConfigureRealMapper();
            var reservationService = new ReservationService(mapper, productRepositoryMock?.Object ?? new Mock<IRepository<Product>>().Object);
            var orderFacade = new OrderFacade(uowMock.Object, null, null, reservationService);
            return orderFacade;
        }

        private static OrderFacade CreateOrderFacade(Mock<IRepository<Product>> productRepositoryMock, Mock<QueryObjectBase<OrderItemDto, OrderItem, OrderItemFilterDto, IQuery<OrderItem>>> orderItemQueryMock, Mock<QueryObjectBase<OrderDto, Order, OrderFilterDto, IQuery<Order>>> orderQueryMock, Mock<IRepository<Order>> orderRepositoryMock, Mock<IRepository<OrderItem>> orderItemRepositoryMock, Mock<IRepository<Customer>> customerRepositoryMock, ReservationService productReservationService = null)
        {
            var uowMock = FacadeMockManager.ConfigureUowMock();
            var mapper = FacadeMockManager.ConfigureRealMapper();
            var salesService = new SalesService(mapper, orderItemQueryMock.Object, orderQueryMock.Object);
            var checkoutService = new CheckoutService(mapper,new List<IPriceCalculator>{new PercentagePriceCalculator(), new Quantity3Plus1PriceCalculator()}, orderRepositoryMock.Object, orderItemRepositoryMock.Object, customerRepositoryMock.Object, productRepositoryMock.Object);
            var reservationService = productReservationService ?? new ReservationService(mapper, productRepositoryMock.Object);
            var orderFacade = new OrderFacade(uowMock.Object, checkoutService, salesService, reservationService);
            return orderFacade;
        }
    }
}
