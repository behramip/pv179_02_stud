﻿using System;
using AutoMapper;
using DemoEshop.BusinessLayer.DataTransferObjects;
using DemoEshop.BusinessLayer.DataTransferObjects.Filters;
using DemoEshop.BusinessLayer.QueryObjects.Common;
using DemoEshop.DataAccessLayer.EntityFramework.Entities;
using DemoEshop.Infrastructure.Query;
using DemoEshop.Infrastructure.Query.Predicates;
using DemoEshop.Infrastructure.Query.Predicates.Operators;

namespace DemoEshop.BusinessLayer.QueryObjects
{
    public class OrderItemQueryObject : QueryObjectBase<OrderItemDto, OrderItem, OrderItemFilterDto, IQuery<OrderItem>>
    {
        public OrderItemQueryObject(IMapper mapper, IQuery<OrderItem> query) : base(mapper, query) { }

        protected override IQuery<OrderItem> ApplyWhereClause(IQuery<OrderItem> query, OrderItemFilterDto filter)
        {
            if (filter.OrderId.Equals(Guid.Empty))
            {
                return query;
            }
            return query.Where(new SimplePredicate(nameof(OrderItem.OrderId), ValueComparingOperator.Equal, filter.OrderId));
        }
    }
}
