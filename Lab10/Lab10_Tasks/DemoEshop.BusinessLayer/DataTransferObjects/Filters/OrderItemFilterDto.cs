﻿using System;
using DemoEshop.BusinessLayer.DataTransferObjects.Common;

namespace DemoEshop.BusinessLayer.DataTransferObjects.Filters
{
    public class OrderItemFilterDto : FilterDtoBase
    {
        public Guid OrderId { get; set; }
    }
}
