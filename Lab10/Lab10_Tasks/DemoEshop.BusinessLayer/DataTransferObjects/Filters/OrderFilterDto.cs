﻿using System;
using DemoEshop.BusinessLayer.DataTransferObjects.Common;

namespace DemoEshop.BusinessLayer.DataTransferObjects.Filters
{
    public class OrderFilterDto : FilterDtoBase
    {
        public Guid CustomerId { get; set; }
    }
}
