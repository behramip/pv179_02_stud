﻿namespace DemoEshop.BusinessLayer.DataTransferObjects.Enums
{
    /// <summary>
    /// Represents type of discount for corresponding product
    /// </summary>
    public enum DiscountType
    {
        Percentage, Quantity3Plus1
    }
}
