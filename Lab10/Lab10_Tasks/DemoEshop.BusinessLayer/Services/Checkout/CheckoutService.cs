﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DemoEshop.BusinessLayer.DataTransferObjects;
using DemoEshop.BusinessLayer.DataTransferObjects.Enums;
using DemoEshop.BusinessLayer.Services.Checkout.PriceCalculators;
using DemoEshop.BusinessLayer.Services.Common;
using DemoEshop.DataAccessLayer.EntityFramework.Entities;
using DemoEshop.Infrastructure;

namespace DemoEshop.BusinessLayer.Services.Checkout
{
    public class CheckoutService : ServiceBase, ICheckoutService
    {
        private readonly IRepository<Order> orderRepository;
        private readonly IRepository<OrderItem> orderItemRepository;
        private readonly IRepository<Customer> customerRepository;
        private readonly IRepository<Product> productRepository;

        /// <summary>
        /// Registered price calculators
        /// </summary>
        private readonly IEnumerable<IPriceCalculator> priceCalculators;

        public CheckoutService(IMapper mapper, IEnumerable<IPriceCalculator> priceCalculators, IRepository<Order> orderRepository,
            IRepository<OrderItem> orderItemRepository, IRepository<Customer> customerRepository, IRepository<Product> productRepository) 
            : base(mapper)
        {
            this.orderRepository = orderRepository;
            this.orderItemRepository = orderItemRepository;
            this.customerRepository = customerRepository;
            this.productRepository = productRepository;
            this.priceCalculators = priceCalculators;
        }

        /// <summary>
        /// Persists order together with all related data
        /// </summary>
        /// <param name="createOrderDto">wrapper for order, orderItems, customer and coupon</param>
        public async Task<Guid> ConfirmOrderAsync(OrderCreateDto createOrderDto)
        {
            var order = Mapper.Map<Order>(createOrderDto.OrderDto);

            var customer = await customerRepository.GetAsync(createOrderDto.OrderDto.CustomerId);
            order.Customer = customer ?? throw new ArgumentException("CheckoutService - CreateOrder(...) Customer must not be null");
            
            orderRepository.Create(order);

            foreach (var orderItemDto in createOrderDto.OrderItems)
            {
                var orderItem = Mapper.Map<OrderItem>(orderItemDto);
                orderItem.Product = await productRepository.GetAsync(orderItemDto.Product.Id);
                orderItem.OrderId = order.Id;
                orderItem.Order = order;
                orderItemRepository.Create(orderItem);
            }
            return order.Id;
            /*  RequestOrderDelivery - for instance a request to https://www.easypost.com/dhl-express-api 
                can be made, corresponding warehouse receives a notification about
                delivery request and schedules delivery
            */
        }

        /// <summary>
        /// Calculates total price for all order items
        /// </summary>
        /// <param name="orderItems">all order items</param>
        /// <returns>Total price for given items</returns>
        public decimal CalculateTotalPrice(ICollection<OrderItemDto> orderItems)
        {
            return orderItems?.Sum(orderItem => ResolvePriceCalculator(orderItem.Product.DiscountType)
                       .CalculatePrice(orderItem)) ?? 0;
        }
        
        private IPriceCalculator ResolvePriceCalculator(DiscountType discountType)
        {
            return priceCalculators.First(calculator => calculator.DiscountType.Equals(discountType));
        }
    }
}
