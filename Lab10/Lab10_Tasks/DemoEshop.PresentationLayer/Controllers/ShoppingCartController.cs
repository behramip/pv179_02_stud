﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using DemoEshop.BusinessLayer.DataTransferObjects;
using DemoEshop.BusinessLayer.Facades;
using DemoEshop.PresentationLayer.Helpers.Attributes;
using DemoEshop.PresentationLayer.Helpers.Cookies;
using DemoEshop.PresentationLayer.Models.ShoppingCart;

namespace DemoEshop.PresentationLayer.Controllers
{
    public class ShoppingCartController : Controller
    {
        const string FakeEmail = "daisy@gmail.com";

        #region ConfigurationProperties

        private const int DefaultShoppingItemQuantity = 1;

        private DateTime RefreshedExpiration => DateTime.Now.AddMinutes(ShoppingCartCookieManager.CookieExpirationInMinutes); 
        
        #endregion

        #region Facades

        public CustomerFacade CustomerFacade { get; set; }

        public OrderFacade OrderFacade { get; set; }

        public ProductFacade ProductFacade { get; set; }

        #endregion
        
        #region ShoppingCartItemsListActionMethods

        public async Task<ActionResult> Index()
        {
            var model = await CreateShoppingCartViewModel();
            return View("ShoppingItemsListView", model);
        }

        public async Task<ActionResult> AddItem(Guid id)
        {
            var product = await ProductFacade.GetProductAsync(id);
            var model = await CreateShoppingCartViewModel();

            // if item for the requested product is already in the shopping cart, take no action  
            if (model.ShoppingCartItems.Any(item => item.Product.Equals(product)))
            {
                return RedirectToAction("Index");
            }

            if (await TryReserveStock(model.Customer, product))
            {
                model.ShoppingCartItems.Add(new OrderItemDto
                {
                    Product = product,
                    Quantity = DefaultShoppingItemQuantity
                });
                Response.SaveAllShoppingCartItems(FakeEmail, model.ShoppingCartItems);
            }

            return RedirectToAction("Index");
        }
    
        public async Task<ActionResult> ClearAllItems()
        {
            var customer = await CustomerFacade.GetCustomerAccordingToEmailAsync(FakeEmail);
            Response.ClearAllShoppingCartItems(FakeEmail);
            OrderFacade.ReleaseReservations(customer.Id);
            return RedirectToAction("Index", "Products");
        }

        [HttpPost]
        [MultiPostAction(Name = "action", Argument = "SaveAndContinueShopping")]
        public async Task<ActionResult> SaveAndContinueShopping(ShoppingCartViewModel model)
        {
            await SaveAllShoppingCartItems(model);
            return RedirectToAction("Index", "Products");
        }

        [HttpPost]
        [MultiPostAction(Name = "action", Argument = "Proceed")]
        public async Task<ActionResult> Proceed(ShoppingCartViewModel model)
        {
            await SaveAllShoppingCartItems(model);
            var newModel = await CreateShoppingCartViewModel(model.ShoppingCartItems);
            return View("ShoppingCartCheckoutView", newModel);
        }

        #endregion

        #region CheckoutActionMethods

        [HttpPost]
        [MultiPostAction(Name = "action", Argument = "ConfirmOrder")]
        public async Task<ActionResult> ConfirmOrder(ShoppingCartViewModel model)
        {
            var createOrderDto = await CreateOrderCreateViewModel();

            await OrderFacade.ConfirmOrderAsync(createOrderDto, () => Response.ClearAllShoppingCartItems(FakeEmail));

            var orderConfirmModel = await CreateOrderConfirmationViewModel(createOrderDto);

            return View("OrderConfirmation", orderConfirmModel);
        }

        #endregion

        #region HelperMethods

        private async Task SaveAllShoppingCartItems(ShoppingCartViewModel model)
        {
            var customer = await CustomerFacade.GetCustomerAccordingToEmailAsync(FakeEmail);
            foreach (var item in model.ShoppingCartItems)
            {
                if (item.Quantity == 0)
                {
                    OrderFacade.ReleaseReservations(customer.Id, item.Product.Id);
                    continue;
                }

                if (!await TryReserveStock(customer, item.Product, item.Quantity))
                {
                    await RetryStockReservation(item, customer);
                }   
            }
            var itemsToSave = model.ShoppingCartItems.Where(item => item.Quantity > 0);
            Response.SaveAllShoppingCartItems(FakeEmail, itemsToSave);
        }

        #region StockReservationMethods

        private async Task<bool> TryReserveStock(CustomerDto customer, ProductDto product, int? desiredQuantity = null)
        {
            var reservation = new ProductReservationDto
            {
                CustomerId = customer.Id,
                ProductId = product.Id,
                ReservedAmount = desiredQuantity ?? DefaultShoppingItemQuantity,
                Expiration = RefreshedExpiration
            };
            var isReservationSuccessfull = await OrderFacade.ReserveProductAsync(reservation);
            return isReservationSuccessfull;
        }

        private async Task RetryStockReservation(OrderItemDto item, CustomerDto customer)
        {
            var maxAvailableUnits = await OrderFacade.GetCurrentlyAvailableUnitsAsync(item.Product.Id);
            if (maxAvailableUnits > 0)
            {
                item.Quantity = maxAvailableUnits;
                await TryReserveStock(customer, item.Product, maxAvailableUnits);
            }
        }

        #endregion

        #region ViewModelCreationMethods

        /// <summary>
        /// Creates ShoppingCartViewModel
        /// </summary>
        /// <param name="shoppingCartItems">Items in the shopping cart</param>
        /// <returns>ShoppingCartViewModel</returns>
        private async Task<ShoppingCartViewModel> CreateShoppingCartViewModel(IList<OrderItemDto> shoppingCartItems = null)
        {
            return new ShoppingCartViewModel
            {
                Customer = await CustomerFacade.GetCustomerAccordingToEmailAsync(FakeEmail),
                ShoppingCartItems = shoppingCartItems ?? Request.GetAllShoppingCartItems(FakeEmail),
                TotalPrice = OrderFacade.CalculateTotalPrice(shoppingCartItems)
            };
        }

        /// <summary>
        /// Creates OrderConfirmationViewModel
        /// </summary>
        /// <param name="createOrderDto">ViewModel containing data about order creation</param>
        /// <returns>OrderConfirmationViewModel</returns>
        private async Task<OrderConfirmationViewModel> CreateOrderConfirmationViewModel(OrderCreateDto createOrderDto)
        {
            return new OrderConfirmationViewModel
            {
                Customer = await CustomerFacade.GetCustomerAccordingToEmailAsync(FakeEmail),
                Order = createOrderDto.OrderDto
            };
        }

        /// <summary>
        /// Creates OrderCreateViewModel
        /// </summary>
        /// <returns>OrderCreateViewModel</returns>
        private async Task<OrderCreateDto> CreateOrderCreateViewModel()
        {
            var newModel = await CreateShoppingCartViewModel();
            
            var order = new OrderDto
            {
                CustomerId = newModel.Customer.Id,
                Issued = DateTime.Now,
                TotalPrice = OrderFacade.CalculateTotalPrice(newModel.ShoppingCartItems)
            };

            return new OrderCreateDto
            {
                OrderDto = order,
                OrderItems = newModel.ShoppingCartItems
            };
        }

        #endregion

        #endregion
    }
}
