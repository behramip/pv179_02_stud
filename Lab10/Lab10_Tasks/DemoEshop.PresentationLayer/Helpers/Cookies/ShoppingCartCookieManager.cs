﻿using System;
using System.Collections.Generic;
using System.Web;
using DemoEshop.BusinessLayer.DataTransferObjects;
using Newtonsoft.Json;

namespace DemoEshop.PresentationLayer.Helpers.Cookies
{
    /// <summary>
    /// Manages cookies used to store shopping cart items
    /// </summary>
    public static class ShoppingCartCookieManager
    {
        #region Constants

        private const string BaseCookieName = "ShoppingCart";

        private const string CookieValue = "ShoppingCartItems";

        public const int CookieExpirationInMinutes = 60;

        #endregion

        #region ShoppingItemsManagement

        /// <summary>
        /// Gets all shopping cart items form cookie within request according to given user email
        /// </summary>
        /// <param name="request">HTTP request containing shopping cart cookie for given user</param>
        /// <param name="customerEmail">Email of the cookie owner</param>
        /// <returns>List of current shopping cart items</returns>
        public static IList<OrderItemDto> GetAllShoppingCartItems(this HttpRequestBase request, string customerEmail)
        {
            var cookie = GetShoppingCartCookie(request, customerEmail);
            if (cookie == null)
            {
                return new List<OrderItemDto>();
            }
            var shoppingCartItemsJson = cookie.Values[CookieValue];
            var items = Deserialize<List<OrderItemDto>>(shoppingCartItemsJson);
            return items ?? new List<OrderItemDto>();
        }

        /// <summary>
        /// Saves given shopping cart items to corresponding cookie
        /// </summary>
        /// <param name="response">HTTP response to save the shopping cart items cookie to</param>
        /// <param name="shoppingCartItems">shopping cart items to save</param>
        /// <param name="customerEmail">Email of the cookie owner</param>
        public static void SaveAllShoppingCartItems(this HttpResponseBase response, string customerEmail, IEnumerable<OrderItemDto> shoppingCartItems = null)
        {
            ShrinkShoppingCartItem(shoppingCartItems);
            var shoppingCartItemsJson = Serialize(shoppingCartItems ?? new List<OrderItemDto>());
            var cookie = GetShoppingCartCookie(response, customerEmail);
            cookie.Expires = DateTime.Now.AddMinutes(CookieExpirationInMinutes);
            cookie[CookieValue] = shoppingCartItemsJson;
        }

        /// <summary>
        /// Clears all shopping cart items
        /// </summary>
        /// <param name="response">HTTP response to save the shopping cart items cookie to</param>
        /// <param name="customerEmail">Email of the cookie owner</param>
        public static void ClearAllShoppingCartItems(this HttpResponseBase response, string customerEmail)
        {
            // Saving null collection will cause shopping cart items cookie clearing
            response.SaveAllShoppingCartItems(customerEmail);
        }

        /// <summary>
        /// Reduces shopping item size, so it does not take much space within the cookie
        /// </summary>
        /// <param name="shoppingCartItems">Shopping cart items to reduce</param>
        private static void ShrinkShoppingCartItem(IEnumerable<OrderItemDto> shoppingCartItems)
        {
            foreach (var shoppingCartItem in shoppingCartItems ?? new List<OrderItemDto>())
            {
                shoppingCartItem.Product.Description = "";
            }
        }

        #endregion

        #region CookieManagement

        /// <summary>
        /// Creates new ShoppingCartCookie for the given HTTP response
        /// </summary>
        /// <param name="response">HTTP response</param>
        /// <param name="customerEmail">Email of the cookie owner</param>
        private static void CreateEshopCartCookieCore(this HttpResponseBase response, string customerEmail)
        {  
            var cookieName = BaseCookieName + customerEmail;
            var cookie = new HttpCookie(cookieName)
            {
                Expires = DateTime.Now.AddMinutes(CookieExpirationInMinutes)
            };
            cookie.Values[CookieValue] = Serialize(new List<OrderItemDto>());
            response.Cookies.Add(cookie);
        }

        #endregion

        #region CookieRetrieval

        /// <summary>
        /// Gets Cookie from request
        /// </summary>
        /// <param name="request">HTTP request containing shopping cart cookie for given user</param>
        /// <param name="customerEmail">Email of the cookie owner</param>
        /// <returns>Corresponding cookie</returns>
        private static HttpCookie GetShoppingCartCookie(this HttpRequestBase request, string customerEmail)
        {
            if (string.IsNullOrEmpty(customerEmail))
            {
                throw new ArgumentException("Customer email cant be null or empty");
            }
            return request.Cookies[BaseCookieName + customerEmail];
        }

        /// <summary>
        /// Gets Cookie from response
        /// </summary>
        /// <param name="response">HTTP response containing shopping cart cookie for given user</param>
        /// <param name="customerEmail">Email of the cookie owner</param>
        /// <returns>Corresponding cookie</returns>
        private static HttpCookie GetShoppingCartCookie(this HttpResponseBase response, string customerEmail)
        {
            if (string.IsNullOrEmpty(customerEmail))
            {
                throw new ArgumentException("Customer email cant be null or empty");
            }
            var cookie = response.Cookies[BaseCookieName + customerEmail];
            if (cookie == null)
            {
                CreateEshopCartCookieCore(response, customerEmail);
                return response.Cookies[BaseCookieName + customerEmail];
            }
            return cookie;
        }

        #endregion

        #region Serialization

        private static string Serialize<T>(T data) where T : class
        {
            return JsonConvert.SerializeObject(data);
        }

        private static T Deserialize<T>(string data) where T : class
        {
            return JsonConvert.DeserializeObject<T>(data);
        } 

        #endregion
    }
}