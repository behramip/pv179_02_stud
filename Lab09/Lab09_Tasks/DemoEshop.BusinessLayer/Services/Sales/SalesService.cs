﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DemoEshop.BusinessLayer.DataTransferObjects;
using DemoEshop.BusinessLayer.DataTransferObjects.Common;
using DemoEshop.BusinessLayer.DataTransferObjects.Filters;
using DemoEshop.BusinessLayer.QueryObjects.Common;
using DemoEshop.BusinessLayer.Services.Common;
using DemoEshop.DataAccessLayer.EntityFramework.Entities;
using DemoEshop.Infrastructure.Query;

namespace DemoEshop.BusinessLayer.Services.Sales
{
    public class SalesService : ServiceBase, ISalesService
    {
        private readonly QueryObjectBase<OrderItemDto, OrderItem, OrderItemFilterDto, IQuery<OrderItem>> orderItemQueryObject;
        private readonly QueryObjectBase<OrderDto, Order, OrderFilterDto, IQuery<Order>> orderQueryObject;


        public SalesService(IMapper mapper, QueryObjectBase<OrderItemDto, OrderItem, OrderItemFilterDto, IQuery<OrderItem>> orderItemQueryObject, QueryObjectBase<OrderDto, Order, OrderFilterDto, IQuery<Order>> orderQueryObject) : base(mapper)
        {
            this.orderItemQueryObject = orderItemQueryObject;
            this.orderQueryObject = orderQueryObject;
        }
        
        /// <summary>
        /// Gets all orders according to filter
        /// </summary>
        /// <param name="filter">order filter</param>
        /// <returns>All orders</returns>
        public async Task<QueryResultDto<OrderDto, OrderFilterDto>> ListOrdersAsync(OrderFilterDto filter)
        {
            return await orderQueryObject.ExecuteQuery(filter);
        }

        /// <summary>
        /// Gets all order items according to filter
        /// </summary>
        /// <param name="filter">order filter</param>
        /// <returns>All orders</returns>
        public async Task<IEnumerable<OrderItemDto>> ListOrderItemsAsync(OrderItemFilterDto filter)
        {
            return (await orderItemQueryObject.ExecuteQuery(filter)).Items;
        }
    }
}
