﻿using System;
using AutoMapper;
using DemoEshop.BusinessLayer.DataTransferObjects;
using DemoEshop.BusinessLayer.DataTransferObjects.Filters;
using DemoEshop.BusinessLayer.QueryObjects.Common;
using DemoEshop.DataAccessLayer.EntityFramework.Entities;
using DemoEshop.Infrastructure.Query;
using DemoEshop.Infrastructure.Query.Predicates;
using DemoEshop.Infrastructure.Query.Predicates.Operators;

namespace DemoEshop.BusinessLayer.QueryObjects
{
    public class OrderQueryObject : QueryObjectBase<OrderDto, Order, OrderFilterDto, IQuery<Order>>
    {
        public OrderQueryObject(IMapper mapper, IQuery<Order> query) : base(mapper, query) { }

        protected override IQuery<Order> ApplyWhereClause(IQuery<Order> query, OrderFilterDto filter)
        {
            return filter.CustomerId.Equals(Guid.Empty)
                ? query
                : query.Where(new SimplePredicate(nameof(Order.CustomerId), ValueComparingOperator.Equal, filter.CustomerId));
        }
    }
}
