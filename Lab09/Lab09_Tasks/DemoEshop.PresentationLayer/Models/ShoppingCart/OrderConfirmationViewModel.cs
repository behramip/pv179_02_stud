﻿using DemoEshop.BusinessLayer.DataTransferObjects;

namespace DemoEshop.PresentationLayer.Models.ShoppingCart
{
    /// <summary>
    /// Wrapper for order confirmation
    /// </summary>
    public class OrderConfirmationViewModel
    {
        public CustomerDto Customer { get; set; }

        public OrderDto Order { get; set; }
    }
}
