﻿using System;
using System.Collections.Generic;
using System.Web;
using DemoEshop.BusinessLayer.DataTransferObjects;
using Newtonsoft.Json;

namespace DemoEshop.PresentationLayer.Helpers.Cookies
{
    /// <summary>
    /// Manages cookies used to store shopping cart items
    /// </summary>
    public static class ShoppingCartCookieManager
    {
        #region Constants

        private const string BaseCookieName = "ShoppingCart";

        private const string CookieValue = "ShoppingCartItems";

        public const int CookieExpirationInMinutes = 60;

        #endregion Constants

        #region ShoppingItemsManagement

        /// <summary>
        /// Gets all shopping cart items form cookie within request according to given user email
        /// </summary>
        /// <param name="request">HTTP request containing shopping cart cookie for given user</param>
        /// <param name="customerEmail">Email of the cookie owner</param>
        /// <returns>List of current shopping cart items</returns>
        public static IList<OrderItemDto> GetAllShoppingCartItems(this HttpRequestBase request, string customerEmail)
        {
            // TODO...

            return new List<OrderItemDto>();
        }

        /// <summary>
        /// Saves given shopping cart items to corresponding cookie, remember that cookie size is (by design) limited
        /// </summary>
        /// <param name="response">HTTP response to save the shopping cart items cookie to</param>
        /// <param name="shoppingCartItems">shopping cart items to save</param>
        /// <param name="customerEmail">Email of the cookie owner</param>
        public static void SaveAllShoppingCartItems(this HttpResponseBase response, string customerEmail, IEnumerable<OrderItemDto> shoppingCartItems = null)
        {
            // TODO...

            throw new NotImplementedException();
        }

        /// <summary>
        /// Clears all shopping cart items
        /// </summary>
        /// <param name="response">HTTP response to save the shopping cart items cookie to</param>
        /// <param name="customerEmail">Email of the cookie owner</param>
        public static void ClearAllShoppingCartItems(this HttpResponseBase response, string customerEmail)
        {
            // TODO...

            throw new NotImplementedException();
        }

        #endregion ShoppingItemsManagement

        #region CookieManagement

        /// <summary>
        /// Creates new ShoppingCartCookie for the given HTTP response
        /// </summary>
        /// <param name="response">HTTP response</param>
        /// <param name="customerEmail">Email of the cookie owner</param>
        private static void CreateEshopCartCookieCore(this HttpResponseBase response, string customerEmail)
        {
            // TODO...

            throw new NotImplementedException();
        }

        #endregion CookieManagement

        #region CookieRetrieval

        /// <summary>
        /// Gets Cookie from request
        /// </summary>
        /// <param name="request">HTTP request containing shopping cart cookie for given user</param>
        /// <param name="customerEmail">Email of the cookie owner</param>
        /// <returns>Corresponding cookie</returns>
        private static HttpCookie GetShoppingCartCookie(this HttpRequestBase request, string customerEmail)
        {
            // TODO...

            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets Cookie from response
        /// </summary>
        /// <param name="response">HTTP response containing shopping cart cookie for given user</param>
        /// <param name="customerEmail">Email of the cookie owner</param>
        /// <returns>Corresponding cookie</returns>
        private static HttpCookie GetShoppingCartCookie(this HttpResponseBase response, string customerEmail)
        {
            // TODO...

            throw new NotImplementedException();
        }

        #endregion CookieRetrieval

        #region Serialization

        private static string Serialize<T>(T data) where T : class
        {
            return JsonConvert.SerializeObject(data);
        }

        private static T Deserialize<T>(string data) where T : class
        {
            return JsonConvert.DeserializeObject<T>(data);
        }

        #endregion Serialization
    }
}