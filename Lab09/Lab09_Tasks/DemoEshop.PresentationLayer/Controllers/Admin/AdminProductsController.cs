﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using DemoEshop.BusinessLayer.DataTransferObjects;
using DemoEshop.BusinessLayer.DataTransferObjects.Filters;
using DemoEshop.BusinessLayer.Facades;
using DemoEshop.PresentationLayer.Models.AdminProducts;
using X.PagedList;

namespace DemoEshop.PresentationLayer.Controllers.Admin
{
    public class AdminProductsController : Controller
    {
        public ProductFacade ProductFacade { get; set; }
        public OrderFacade OrderFacade { get; set; }

        public async Task<ActionResult> Index(int page = 1)
        {
            // TODO...

            throw new NotImplementedException();
        }
        
        public ActionResult Create()
        {
            // TODO...

            throw new NotImplementedException();
        }

        public async Task<ActionResult> Create(AdminProductEditModel model)
        {
            // TODO...

            throw new NotImplementedException();
        }

        public async Task<ActionResult> Edit(Guid id)
        {
            // TODO...

            throw new NotImplementedException();
        }

        public async Task<ActionResult> Edit(Guid id, AdminProductEditModel model)
        {
            // TODO...

            throw new NotImplementedException();
        }

        public async Task<ActionResult> Delete(Guid id)
        {
            // TODO...

            throw new NotImplementedException();
        }
    }
}
