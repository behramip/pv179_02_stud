﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DemoEshop.Infrastructure.Query.Predicates;
using DemoEshop.Infrastructure.Query.Predicates.Operators;

namespace DemoEshop.Infrastructure.EntityFramework
{
    public static class SimplePredicateExtension
    {
        /// <summary>
        /// Helper dictionary used to create Binary expressions from member expression and constatnexpression
        /// </summary>
        private static readonly IDictionary<ValueComparingOperator, Func<MemberExpression, ConstantExpression, Expression>> Expressions = 
            new Dictionary<ValueComparingOperator, Func<MemberExpression, ConstantExpression, Expression>>
        {
            {ValueComparingOperator.Equal, Expression.Equal },
            {ValueComparingOperator.NotEqual, Expression.NotEqual },
            {ValueComparingOperator.GreaterThan, Expression.GreaterThan },
            {ValueComparingOperator.GreaterThanOrEqual, Expression.GreaterThanOrEqual },
            {ValueComparingOperator.LessThan, Expression.LessThan },
            {ValueComparingOperator.LessThanOrEqual, Expression.LessThanOrEqual },
            {ValueComparingOperator.StringContains, (memberExpression, constantExpression) => 
                Expression.Call(memberExpression, typeof(string).GetMethod("Contains", new[] { typeof(string) }), constantExpression)}
        };
        
        /// <summary>
        /// Gets binary expression from given simple predicate and parameter expression
        /// </summary>
        /// <param name="simplePredicate">Simple predicate containing name of the compared property, compared value and binary operator</param>
        /// <param name="parameterExpression">Shared parameter expression</param>
        /// <returns></returns>
        public static Expression GetExpression(this SimplePredicate simplePredicate, ParameterExpression parameterExpression)
        {
            // TODO...

            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the (expected) type of the compared value from simple predicate
        /// </summary>
        /// <param name="simplePredicate"></param>
        /// <param name="memberExpression"></param>
        /// <returns></returns>
        private static Type GetMemberType(SimplePredicate simplePredicate, MemberExpression memberExpression)
        {
            return memberExpression.Member.DeclaringType?.GetProperty(simplePredicate.TargetPropertyName)?.PropertyType;
        }
    }
}
