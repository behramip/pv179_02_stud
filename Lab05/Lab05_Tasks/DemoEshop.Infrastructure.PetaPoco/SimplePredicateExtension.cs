﻿using System;
using System.Collections.Generic;
using DemoEshop.Infrastructure.Query.Predicates;
using DemoEshop.Infrastructure.Query.Predicates.Operators;

namespace DemoEshop.Infrastructure.PetaPoco
{
    public static class SimplePredicateExtension
    {
        /// <summary>
        /// Helper dictionary for formatting the operator and right operand part of binary predicate.
        /// Given simple predicate, stating Price GreaterThan 1000 returns: "> 1000"
        /// </summary>
        private static readonly IDictionary<ValueComparingOperator, Func<object, string>> BinaryOperations =
            new Dictionary<ValueComparingOperator, Func<object, string>>
            {
                {ValueComparingOperator.Equal, rightOperand => rightOperand is string || rightOperand is Guid ? $" = '{rightOperand}'" : $" = {rightOperand}" },
                {ValueComparingOperator.NotEqual, rightOperand => rightOperand is string || rightOperand is Guid ? $" != '{rightOperand}'" : $" != {rightOperand}" },
                {ValueComparingOperator.GreaterThan, rightOperand => $" > {rightOperand}" },
                {ValueComparingOperator.GreaterThanOrEqual, rightOperand => $" >= {rightOperand}" },
                {ValueComparingOperator.LessThan, rightOperand => $" < {rightOperand}" },
                {ValueComparingOperator.LessThanOrEqual, rightOperand => $" <= {rightOperand}" },
                {ValueComparingOperator.StringContains, rightOperand => $" LIKE '%{rightOperand}%'"}
            };

        /// <summary>
        /// Creates binary condition from given simple predicate (e.g. Price >= 1000),
        /// do not forget that some characters, namely the "at-sign" ("@") must be
        /// escaped ("@@") in order to interpret the query correctly.
        /// </summary>
        /// <param name="simplePredicate"></param>
        /// <returns></returns>
        public static string GetWhereCondition(this SimplePredicate simplePredicate)
        {
            // TODO...

            throw new NotImplementedException();
        }
    }
}
