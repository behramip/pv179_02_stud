﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using AsyncPoco;
using DemoEshop.Infrastructure.PetaPoco.UnitOfWork;
using DemoEshop.Infrastructure.Query;
using DemoEshop.Infrastructure.Query.Predicates;
using DemoEshop.Infrastructure.Query.Predicates.Operators;
using DemoEshop.Infrastructure.UnitOfWork;

namespace DemoEshop.Infrastructure.PetaPoco
{
    public class PetaPocoQuery<TEntity> : QueryBase<TEntity> where TEntity : class, IEntity, new()
    {
        #region SQLStatements
        private const string SelectFromClause = "SELECT * FROM ";
        private const string WhereClause = "WHERE ";
        private const string OrderByClause = "ORDER BY ";
        private const string Ascending = " ASC";
        private const string Descending = " DESC";
        private const string Or = " OR ";
        private const string And = " AND ";
        private const string OpenParenthesis = "(";
        private const string CloseParenthesis = ")";
        #endregion       

        /// <summary>
        /// Gets the <see cref="IDatabase"/>.
        /// </summary>
        protected IDatabase Database => ((PetaPocoUnitOfWork)Provider.GetUnitOfWorkInstance()).Database;

        public PetaPocoQuery(IUnitOfWorkProvider provider) : base(provider) { }

        public override async Task<QueryResult<TEntity>> ExecuteAsync()
        {
            // TODO...

            throw new NotImplementedException();
        }
    }
}
