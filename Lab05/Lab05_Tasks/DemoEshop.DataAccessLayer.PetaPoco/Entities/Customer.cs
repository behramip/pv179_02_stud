﻿using System;
using AsyncPoco;
using DemoEshop.Infrastructure;

namespace DemoEshop.DataAccessLayer.PetaPoco.Entities
{
    /// <summary>
    /// Represents eshop customer
    /// </summary>
    [TableName(TableNames.CustomerTable)]
    [PrimaryKey("Id", autoIncrement = false)]
    public class Customer : IEntity
    {        
        public Guid Id { get; set; }

        [Ignore]
        public string TableName { get; } = TableNames.CustomerTable;

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string MobilePhoneNumber { get; set; }

        public string Address { get; set; }

        public DateTime BirthDate { get; set; } = new DateTime(1950, 1, 1);
    }
}