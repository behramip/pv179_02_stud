﻿using System;
using System.Threading.Tasks;
using DemoEshop.DataAccessLayer.EntityFramework.Entities;
using DemoEshop.Infrastructure;
using DemoEshop.Infrastructure.EntityFramework;
using NUnit.Framework;

namespace DemoEshop.DataAccessLayer.EntityFramework.Tests.RepositoryTests
{
    [TestFixture]
    public class CategoryRepositoryTests
    {
        private readonly IRepository<Category> categoryRepository = new EntityFrameworkRepository<Category>(Initializer.Provider);

        private readonly Guid smartphonesCategoryId = Guid.Parse("aa01dc64-5c07-40fe-a916-175165b9b90f");

        private readonly Guid androidCategoryId = Guid.Parse("aa02dc64-5c07-40fe-a916-175165b9b90f");

        [Test]
        public async Task GetCategoryAsync_AlreadyStoredInDBNoIncludes_ReturnsCorrectCategory()
        {
            // Arrange
            Category androidCategory;

            // Act
            using (Initializer.Provider.Create())
            {
                androidCategory = await categoryRepository.GetAsync(androidCategoryId);
            }

            // Assert
            Assert.AreEqual(androidCategory.Id, androidCategoryId);
        }

        [Test]
        public async Task GetCategoryAsync_AlreadyStoredInDBWithIncludes_ReturnsCorrectCategoryWithInitializedParent()
        {
            Category androidCategory;

            using (Initializer.Provider.Create())
            {
                androidCategory = await categoryRepository.GetAsync(androidCategoryId, nameof(Category.Parent));
            }

            Assert.IsTrue(androidCategory.Id.Equals(androidCategoryId) && androidCategory.Parent.Id.Equals(smartphonesCategoryId));
        }

        [Test]
        public async Task CreateCategoryAsync_CategoryIsNotPreviouslySeeded_CreatesNewCategory()
        {
            var windows10Mobile = new Category { Name = "Windows 10", ParentId = smartphonesCategoryId };

            using (var uow = Initializer.Provider.Create())
            {
                categoryRepository.Create(windows10Mobile);
                await uow.Commit();
                
            }
            Assert.IsTrue(!windows10Mobile.Id.Equals(Guid.Empty));
        }

        [Test]
        public async Task UpdateCategoryAsync_CategoryIsPreviouslySeeded_UpdatesCategory()
        {
            Category updatedAndroidCategory;
            var newAndroidCategory = new Category { Id = androidCategoryId, Name = "Updated Name", ParentId = null };

            using (var uow = Initializer.Provider.Create())
            {
                categoryRepository.Update(newAndroidCategory);
                await uow.Commit();
                updatedAndroidCategory = await categoryRepository.GetAsync(androidCategoryId);
            }

            Assert.IsTrue(newAndroidCategory.Name.Equals(updatedAndroidCategory.Name) && newAndroidCategory.ParentId.Equals(null));
        }

        [Test]
        public async Task DeleteCategoryAsync_CategoryIsPreviouslySeeded_DeletesCategory()
        {
            Category deletedAndroidCategory;

            using (var uow = Initializer.Provider.Create())
            {
                categoryRepository.Delete(androidCategoryId);
                await uow.Commit();
                deletedAndroidCategory = await categoryRepository.GetAsync(androidCategoryId);
            }

            Assert.AreEqual(deletedAndroidCategory, null);
        }
    }
}
