﻿using System;
using System.Threading.Tasks;
using DemoEshop.DataAccessLayer.EntityFramework.Entities;
using DemoEshop.DataAccessLayer.EntityFramework.Enums;
using DemoEshop.Infrastructure;
using DemoEshop.Infrastructure.EntityFramework;
using NUnit.Framework;

namespace DemoEshop.DataAccessLayer.EntityFramework.Tests.RepositoryTests
{
    public class ProductRepositoryTests
    {
        // TODO: uncomment
        //private readonly IRepository<Product> productRepository = new EntityFrameworkRepository<Product>(Initializer.Provider);

        private readonly Guid androidCategoryId = Guid.Parse("aa02dc64-5c07-40fe-a916-175165b9b90f");

        private readonly Guid samsungGalaxyJ7Id = Guid.Parse("aa05dc64-5c07-40fe-a916-175165b9b90f");

        private readonly Guid xiaomiMi5Id = Guid.Parse("aa12dc64-5c07-40fe-a916-175165b9b90f");

        [Test]
        public async Task GetProductAsync_AlreadyStoredInDBNoIncludes_ReturnsCorrectProduct()
        {
            // TODO...
        }

        [Test]
        public async Task GetProductAsync_AlreadyStoredInDBWithIncludes_ReturnsCorrectProductWithInitializedParent()
        {
            // TODO...
        }

        [Test]
        public async Task CreateProductAsync_ProductIsNotPreviouslySeeded_CreatesNewProduct()
        {
            // TODO...
        }


        [Test]
        public async Task UpdateProductAsync_ProductIsPreviouslySeeded_UpdatesProduct()
        {
            // TODO...
        }

        [Test]
        public async Task DeleteProductAsync_ProductIsPreviouslySeeded_DeletesProduct()
        {
            // TODO...
        }
    }
}
