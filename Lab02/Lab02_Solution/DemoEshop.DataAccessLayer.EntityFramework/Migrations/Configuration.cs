using System;
using System.Data.Entity.Migrations;
using DemoEshop.DataAccessLayer.EntityFramework.Entities;
using DemoEshop.DataAccessLayer.EntityFramework.Enums;

namespace DemoEshop.DataAccessLayer.EntityFramework.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Lab01DemoEshopDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Lab01DemoEshopDbContext context)
        {
            // Configure case invariant comparison for category and product names
            context.Database.ExecuteSqlCommand(
                "ALTER TABLE Products ALTER COLUMN Name VARCHAR(256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL");
            context.Database.ExecuteSqlCommand(
                "ALTER TABLE Categories ALTER COLUMN Name VARCHAR(256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL");

            var smartphones = new Category { Id = Guid.Parse("aa01dc64-5c07-40fe-a916-175165b9b90f"), Name = "Smartphones", Parent = null, ParentId = null };

            var android = new Category { Id = Guid.Parse("aa02dc64-5c07-40fe-a916-175165b9b90f"), Name = "Android", Parent = smartphones, ParentId = smartphones.Id };

            var windows10Mobile = new Category { Id = Guid.Parse("aa03dc64-5c07-40fe-a916-175165b9b90f"), Name = "Windows 10", Parent = smartphones, ParentId = smartphones.Id };

            context.Categories.AddOrUpdate(category => category.Id, smartphones, android, windows10Mobile);

            var samsungGalaxyJ7 = new Product
            {
                Id = Guid.Parse("aa05dc64-5c07-40fe-a916-175165b9b90f"),
                Category = android,
                CategoryId = android.Id,
                Description = "Designed with all the features you love, the Samsung Galaxy J7 is the smartphone you�ve been waiting for. Watching a movie or reading a book is more enjoyable on the large 5.5 HD Super AMOLED display.And while the 13MP main camera captures clearer photos, the 5MP front camera gives you more flattering selfies in any light. Best of all, the long-lasting battery means the Samsung Galaxy J7 keeps up with you.",
                StoredUnits = 6,
                DiscountPercentage = 5,
                DiscountType = DiscountType.Percentage,
                Name = "Samsung Galaxy J7",
                Price = 7490,
                ProductImgUri = @"\Content\Images\Products\samsung_galaxy_J7.jpeg"
            };
            
            var lumia950XL = new Product
            {
                Id = Guid.Parse("aa07dc64-5c07-40fe-a916-175165b9b90f"),
                Category = windows10Mobile,
                CategoryId = windows10Mobile.Id,
                Description = "Microsoft Lumia 950 XL smartphone was launched in October 2015. The phone comes with a 5.70-inch touchscreen display with a resolution of 1440 pixels by 2560 pixels at a PPI of 518 pixels per inch. The Microsoft Lumia 950 XL is powered by octa - core Qualcomm Snapdragon 810 processor and it comes with 3GB of RAM.The phone packs 32GB of internal storage that can be expanded up to 200GB via a microSD card.As far as the cameras are concerned, the Microsoft Lumia 950 XL packs a 20-megapixel primary camera on the rear and a 5-megapixel front shooter for selfies. The Microsoft Lumia 950 XL runs Windows 10 Mobile and is powered by a 3340mAh removable battery.It measures 151.90 x 78.40 x 8.10 (height x width x thickness) and weighs 165.00 grams. The Microsoft Lumia 950 XL is a single SIM(GSM) smartphone that accepts a Nano-SIM.Connectivity options include Wi-Fi, GPS, Bluetooth, NFC, FM, 4G(with support for Band 40 used by some LTE networks in India). Sensors on the phone include Proximity sensor, Ambient light sensor, Accelerometer, and Gyroscope. ",
                StoredUnits = 8,
                DiscountPercentage = 0,
                DiscountType = DiscountType.Percentage,
                Name = "Microsoft Lumia 950XL",
                Price = 16490,
                ProductImgUri = @"\Content\Images\Products\microsoft_lumia_950_xl.jpeg"
            };
            
            var htc10 = new Product
            {
                Id = Guid.Parse("aa09dc64-5c07-40fe-a916-175165b9b90f"),
                Category = android,
                CategoryId = android.Id,
                Description = "HTC 10 smartphone was launched in April 2016. The phone comes with a 5.20-inch touchscreen display with a resolution of 1440 pixels by 2560 pixels at a PPI of 564 pixels per inch. The HTC 10 is powered by 1.6GHz quad - core Qualcomm Snapdragon 820 processor and it comes with 4GB of RAM.The phone packs 32GB of internal storage that can be expanded up to 2000GB via a microSD card. As far as the cameras are concerned, the HTC 10 packs a 12-Ultrapixel primary camera on the rear and a 5-megapixel front shooter for selfies.The HTC 10 runs Android 6 and is powered by a 3000mAh non removable battery.It measures 145.90 x 71.90 x 9.00 (height x width x thickness) and weighs 161.00 grams. The HTC 10 is a single SIM(GSM) smartphone that accepts a Nano-SIM.Connectivity options include Wi-Fi, GPS, Bluetooth, NFC, 4G(with support for Band 40 used by some LTE networks in India). Sensors on the phone include Proximity sensor, Ambient light sensor, Accelerometer, and Gyroscope. ",
                StoredUnits = 1,
                DiscountPercentage = 0,
                DiscountType = DiscountType.Percentage,
                Name = "HTC 10",
                Price = 21990,
                ProductImgUri = @"\Content\Images\Products\HTC_10.jpg"
            };
            
            context.Products.AddOrUpdate(product => product.Id, samsungGalaxyJ7, htc10, lumia950XL);

            context.SaveChanges();
        }
    }
}
