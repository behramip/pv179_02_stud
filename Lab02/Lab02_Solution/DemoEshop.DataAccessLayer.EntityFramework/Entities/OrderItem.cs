﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DemoEshop.DataAccessLayer.EntityFramework.Entities
{
    /// <summary>
    /// Single item within customer order
    /// </summary>
    public class OrderItem : IEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        [NotMapped]
        public string TableName { get; } = nameof(Lab01DemoEshopDbContext.OrderItems);

        [Range(0, int.MaxValue)]
        public int Quantity { get; set; }

        [ForeignKey(nameof(Product))]
        public Guid ProductId { get; set; }

        public virtual Product Product { get; set; }

        [ForeignKey(nameof(Order))]
        public Guid OrderId { get; set; }

        public virtual Order Order { get; set; }
    }
}
