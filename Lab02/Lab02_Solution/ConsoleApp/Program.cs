﻿using System;
using System.Collections.Generic;
using System.Linq;
using DemoEshop.DataAccessLayer.EntityFramework;
using DemoEshop.DataAccessLayer.EntityFramework.Entities;

namespace ConsoleApp
{
    class Program
    {
        private static readonly Guid AndroidCategoryId = Guid.Parse("aa02dc64-5c07-40fe-a916-175165b9b90f");

        static void Main(string[] args)
        {
            EagerLoadProducts();
            PerformServerSideQuery();
            PerformServerSideQueryWithIsInRange();
        }

        /// <summary>
        /// Eagerly load products with its categories
        /// </summary>
        private static void EagerLoadProducts()
        {
            IList<Product> products;
            using (var context = new Lab01DemoEshopDbContext())
            {
                products = context.Products.Include(nameof(Product.Category)).ToList();
            }
            var categories = products.Select(product => product.Category).Distinct().ToList();
            foreach (var category in categories)
            {
                Console.WriteLine(category.Name);
            }
        }

        /// <summary>
        /// Find all android smartphones, within price range 7_000 to 22_000 CZK
        /// and return them ordered by price ascending.
        /// Perform this query entirely on the server side
        /// </summary>
        private static void PerformServerSideQuery()
        {
            IList<Product> products;
            using (var context = new Lab01DemoEshopDbContext())
            {
                products = context.Products
                    .Where(product => product.CategoryId.Equals(AndroidCategoryId))
                    .Where(product => product.Price >= 7_000m && product.Price <= 22_000m)
                    .OrderBy(product => product.Price)
                    .ToList();
            }
            foreach (var product in products)
            {
                Console.WriteLine(product.Name);
            }
        }

        /// <summary>
        /// Same task as in previous method, only this time use IsInRangeExtension 
        /// method for decimal type and see what happens :)
        /// </summary>
        private static void PerformServerSideQueryWithIsInRange()
        {
            try
            {
                using (var context = new Lab01DemoEshopDbContext())
                {
                    var products = context.Products
                        .Where(product => product.CategoryId.Equals(AndroidCategoryId))
                        .Where(product => product.Price.IsInRange(7_000m, 22_000m))
                        .OrderBy(product => product.Price)
                        .ToList();
                }
            }
            catch (NotSupportedException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
