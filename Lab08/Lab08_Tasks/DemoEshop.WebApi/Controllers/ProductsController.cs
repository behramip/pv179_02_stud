﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using DemoEshop.BusinessLayer.DataTransferObjects;
using DemoEshop.BusinessLayer.DataTransferObjects.Filters;
using DemoEshop.BusinessLayer.Facades;
using DemoEshop.WebApi.Models.Products;

namespace DemoEshop.WebApi.Controllers
{
    /// <summary>
    /// Ensure DB is seeded, before testing the API
    /// </summary>
    public class ProductsController : ApiController
    {
        public ProductFacade ProductFacade { get; set; }

        /// <summary>
        /// Query products according to given query parameters, example URL call: http://localhost:56118/api/Products/Query?sort=name&asc=true&name=Samsung&minimalPrice=5000&maximalPrice=23000&category=android&category=ios
        /// </summary>
        /// <param name="sort">Name of the product attribute (e.g. "name", "price", ...) to sort according to</param>
        /// <param name="asc">Sort product collection in ascending manner</param>
        /// <param name="name">Product name (can be only partial: "Galaxy", "Lumia", ...)</param>
        /// <param name="minimalPrice">Minimal product price</param>
        /// <param name="maximalPrice">Maximal product price</param>
        /// <param name="category">Product category names, currently supported are: "android", "windows 10" and "ios"</param>
        /// <returns>Collection of products, satisfying given query parameters.</returns>       
        [HttpGet, Route("api/Products/Query")]
        public async Task<IEnumerable<ProductDto>> Query(string sort = null, bool asc = true, 
            string name = null, decimal minimalPrice = 0m, decimal maximalPrice = decimal.MaxValue, 
            [FromUri] string[] category = null)
        {
            // TODO...

            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets product info (including currently available units) for product with given name, 
        /// example URL call: http://localhost:56118/api/Products/Get?name=lg
        /// </summary>
        /// <param name="name">Product name ("microsoft lumia 650", ...)</param>
        /// <returns>Complete product info.</returns>
        public async Task<ProductDto> Get(string name)
        {
            // TODO...

            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates product, example URL call can be found in test folder.
        /// </summary>
        /// <param name="model">Created product details.</param>
        /// <returns>Message describing the action result.</returns>
        public async Task<string> Post([FromBody]ProductCreateModel model)
        {
            // TODO...

            throw new NotImplementedException();
        }

        /// <summary>
        /// Updates product with given id, example URL call can be found in test folder.
        /// </summary>
        /// <param name="id">Id of the product to update.</param>
        /// <param name="product">Product to update</param>
        /// <returns>Message describing the action result.</returns>
        public async Task<string> Put(Guid id, [FromBody]ProductDto product)
        {
            // TODO...

            throw new NotImplementedException();
        }

        /// <summary>
        /// Deletes product with given id ("aa05dc64-5c07-40fe-a916-175165b9b90f", "aa06dc64-5c07-40fe-a916-175165b9b90f", ...),
        /// example URL call can be found in test folder.
        /// </summary>
        /// <param name="id">Id of the product to delete.</param>
        /// <returns>Message describing the action result.</returns>
        public async Task<string> Delete(Guid id)
        {
            // TODO...

            throw new NotImplementedException();
        }
    }
}
