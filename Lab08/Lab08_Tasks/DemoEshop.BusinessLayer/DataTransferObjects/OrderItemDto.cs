﻿using System;
using DemoEshop.BusinessLayer.DataTransferObjects.Common;

namespace DemoEshop.BusinessLayer.DataTransferObjects
{
    /// <summary>
    /// Wrapper class for single item within customer order
    /// </summary>
    public class OrderItemDto : DtoBase
    {
        public int Quantity { get; set; }

        public ProductDto Product { get; set; }

        

        protected bool Equals(OrderItemDto other)
        {
            if (!Id.Equals(Guid.Empty))
            {
                return this.Id == other.Id;
            }
            return Quantity == other.Quantity && 
                Equals(Product, other.Product);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            return obj.GetType() == this.GetType() &&
                Equals((OrderItemDto) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Id.GetHashCode();
                hashCode = (hashCode*397) ^ Quantity;
                hashCode = (hashCode*397) ^ (Product?.GetHashCode() ?? 0);
                return hashCode;
            }
        }

        public override string ToString()
        {
            return $"{Product} {Quantity}x";
        }
    }
}
