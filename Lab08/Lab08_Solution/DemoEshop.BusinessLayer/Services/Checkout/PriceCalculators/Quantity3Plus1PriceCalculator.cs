﻿using DemoEshop.BusinessLayer.DataTransferObjects;
using DemoEshop.BusinessLayer.DataTransferObjects.Enums;

namespace DemoEshop.BusinessLayer.Services.Checkout.PriceCalculators
{
    public class Quantity3Plus1PriceCalculator : IPriceCalculator
    {
        private const int RequiredForDiscount = 4;

        public DiscountType DiscountType { get; } = DiscountType.Quantity3Plus1;

        public decimal CalculatePrice(OrderItemDto orderItem)
        {
            var numberOfFreeProducts = orderItem.Quantity / RequiredForDiscount;
            return (orderItem.Quantity - numberOfFreeProducts) * orderItem.Product.Price;
        }
    }
}
