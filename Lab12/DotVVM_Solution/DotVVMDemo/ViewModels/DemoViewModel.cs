using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotVVM.Framework.ViewModel;
using PseudoBL.DTOs;

namespace DotVVMDemo.ViewModels
{
    public class DemoViewModel : DotvvmViewModelBase
    {
        public QuestionDTO Question { get; set; }
        public List<string> Students { get; set; }

        public int Number { get; set; }
        public string Name { get; set; } = "Martin";
        public int NameLength => Name.Length;
        public int NameACount { get; set; }
        public bool ButtonEnabled { get; set; } = true;

        public DemoViewModel()
        {
            Number = 5;
            Question = new QuestionDTO("1 + 2 = ?",
                "3", "1", "2", "3");
            Students = new List<string>
            {
                "Jaroslav",
                "Martin"
            };
        }

        public void IncreaseNumber()
        {
            Number++;
        }

        public void GetNameACount(string text)
        {
            NameACount = text.Count(t => t == 'a');
        }
    }
}

