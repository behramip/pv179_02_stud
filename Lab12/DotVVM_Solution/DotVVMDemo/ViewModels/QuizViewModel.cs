using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotVVM.Framework.ViewModel;
using PseudoBL.DTOs;
using PseudoBL.Facades;

namespace DotVVMDemo.ViewModels
{
    public class QuizViewModel : DotvvmViewModelBase
    {
        private CategoryFacade categoryFacade;

        public List<CategoryDTO> Categories { get; set; }
        public int SelectedCategoryId { get; set; }

        public List<QuestionAnswerDTO> Questions { get; set; }
        public int Points { get; set; }
        public bool CorrectButtonEnabled { get; set; }
        public bool VisiblePoints { get; set; }

        public QuizViewModel()
        {
            categoryFacade = new CategoryFacade();
            Categories = categoryFacade.GetAllCategories();
        }

        public void ShowQuestions()
        {
            VisiblePoints = false;
            if (SelectedCategoryId != 0)
            {
                CorrectButtonEnabled = true;
            }
            Points = 0;
            categoryFacade = new CategoryFacade();
            var questions = categoryFacade.GetQuestions(SelectedCategoryId);
            if (questions == null)
            {
                Questions = null;
                return;
            }

            Questions = new List<QuestionAnswerDTO>();
            foreach (var question in questions)
            {
                Questions.Add(new QuestionAnswerDTO
                {
                    QuestionDto = question
                });
            }
        }

        public void CorrectAnswers()
        {
            VisiblePoints = true;
            CorrectButtonEnabled = false;
            foreach (var question in Questions.Where(question => question.ChosenAnswer == question.QuestionDto.CorrectAnswer))
            {
                Points++;
            }
        }
    }
}

