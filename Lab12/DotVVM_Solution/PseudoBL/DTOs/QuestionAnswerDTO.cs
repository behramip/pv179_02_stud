﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PseudoBL.DTOs
{
    public class QuestionAnswerDTO
    {
        public QuestionDTO QuestionDto { get; set; }
        public string ChosenAnswer { get; set; }
    }
}
