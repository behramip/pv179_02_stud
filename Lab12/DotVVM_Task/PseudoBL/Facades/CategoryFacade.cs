﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PseudoBL.DTOs;

namespace PseudoBL.Facades
{
    public class CategoryFacade
    {
        /// <summary>
        /// Gets all categories.
        /// </summary>
        /// <returns>All categories</returns>
        public List<CategoryDTO> GetAllCategories()
        {
            return new List<CategoryDTO>
            {
                new CategoryDTO
                {
                    Id = 1,
                    Name = "Math"
                },
                new CategoryDTO
                {
                    Id = 2,
                    Name = "Biology"
                }
            };
        }

        /// <summary>
        /// Gets all questions from given category.
        /// </summary>
        /// <param name="categoryId">category id</param>
        /// <returns>All questions from given category</returns>
        public List<QuestionDTO> GetQuestions(int categoryId)
        {
            switch (categoryId)
            {
                case 1:
                    return new List<QuestionDTO>
                {
                    new QuestionDTO("1 + 1 = ", "2", "5", "2", "8"),
                    new QuestionDTO("6 * 2 = ", "12", "12", "14", "16", "18"),
                    new QuestionDTO("10 / 2 = ", "5", "1", "2", "3", "4", "5")
                };
                case 2:
                    return new List<QuestionDTO>
                    {
                        new QuestionDTO("How many bones are in adult human body?", "206", "256", "207", "206", "203"),
                        new QuestionDTO("Which chemical is synthesized by some sponges and acts as an antibiotic?",
                                        "cribrostatin", "streptomycin", "spongin", "cribrostatin")
                    };
                default:
                    return null;
            }
        }
    }
}
