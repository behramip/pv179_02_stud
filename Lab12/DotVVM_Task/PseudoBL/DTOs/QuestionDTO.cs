﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PseudoBL.DTOs
{
    public class QuestionDTO
    {
        public QuestionDTO() { }
        public QuestionDTO(string text, string correctAnswer, params string[] answers)
        {
            Text = text;
            CorrectAnswer = correctAnswer;
            Answers.AddRange(answers);
        }

        public string Text { get; set; }
        public string CorrectAnswer { get; set; }
        public List<string> Answers { get; set; } = new List<string>();
    }
}
